### 3. Khái niệm về tủ đĩa: SAN storage: Pool, volume, LUN, disk

#### 3.1. Tìm hiểu về SAN - Storage Area Network ( Hệ thống lưu trữ mạng)

**a. Khái niệm**

Lưu trữ mạng: Là phương pháp truy cập dữ liệu trên mạng tương tự như quá trình truyền dữ liệu qua các thiết bị trên máy chủ như : Disks Drivers: ATA, SCSI.

( SCSI là loại HDD được dùng đầu tiên trong Server (Raid), tốc độ nhanh hơn, có thể gom HDD thành một ổ lớn hoặc chia ổ để backup)

( ATA - ổ đĩa IDE, gặp vấn đề trong quá trình xử lý và đọc ghi dữ liệu)

* SAN là kiến ​​trúc mạng lưu trữ phổ biến nhất được sử dụng bởi các doanh nghiệp cho các ứng dụng quan trọng trong kinh doanh, cần cung cấp thông lượng cao và độ trễ thấp.
* SAN là bộ lưu trữ dựa trên khối (block-storage), tận dụng kiến ​​trúc tốc độ cao kết nối các máy chủ với các đơn vị đĩa logic (LUN) của chúng.
* SAN có tính sẵn sàng và khả năng phục hồi cao. Một SAN được thiết kế tốt có thể dễ dàng chịu được nhiều lỗi thành phần hoặc thiết bị.

**b. Lợi ích khi sử dụng SAN**

* Dễ dàng chia sẻ lưu trữ và quản lý thông tin, cho phép nhiều máy chủ cùng chia sẻ một thiết bị lưu trữ.
* Ứng dụng cho hệ thống Data Center và các Cluster.
* Quá trình quản lý của SAN sử dụng NAS (Network Attached Storage) cho phép nhiều máy tính truy cập cùng 1 file trên 1 mạng. -> tạo nên 1 hệ thống lưu trữ thông tin hoàn thiện.
* SAN: cho phép máy tính khởi động trực tiếp từ SAN mà chúng quản lý -> Giúp dễ dàng thay các máy chủ bị lỗi khi đang sử dụng, cấu hình lại, nâng cấp mà không ảnh hưởng tới máy chủ.
* Cung cấp giải pháp khôi phục dữ liệu nhanh chóng khi các thiết bị lưu trữ bị lỗi hoặc không truy cập được.
* Cho phép duplicate - sao chép một tập tin tại 2 vùng vật lý khác nhau (clone) -> Khôi phục dữ liệu nhanh chóng.

**c. Tính năng**

* Lưu trữ được truy cập theo Block qua SCSI ( hầu hết hệ thống SAN hiện nay đều sd SCSI dựa trên hệ thống cáp quang để truyền dữ liệu, không thông qua các Bus hệ thống).
* Khả năng I/O với tốc độ cao
* Tách biệt thiết bị lưu trữ và máy chủ
* Dễ dàng chia sẻ lưu trữ và quản lý thông tin
* Mở rộng lưu trữ dễ dàng
* Cho phép nhiều máy chủ chia sẻ một thiết bị lưu trữ
* Thay đổi, nâng cấp dễ dàng không ảnh hưởng tới hệ thống

**Các loại của SAN**

* Có 4 loại:

1. Fibre Channel Protocol (FCP): được triển khai trong 70% đến 80% tổng thị trường SAN. FCP sử dụng các giao thức truyền tải Kênh với các lệnh SCSI nhúng.
1. Internet Small Computer System Interface (iSCSI): 10% đến 15% thị trường. iSCSI đóng gói các lệnh SCSI bên trong khung Ethernet và sau đó sử dụng mạng Ethernet IP để truyền tải.
1. Fibre Channel over Ethernet (FCoE): chưa đến 5% thị trường SAN. Nó tương tự như iSCSI, vì nó gói gọn một khung FC bên trong một datagram Ethernet. Sau đó, giống như iSCSI, nó sử dụng mạng IP Ethernet để truyền tải.
1. Non-Volatile Memory Express over Fibre Channel (FC-NVMe): là một giao thức giao diện để truy cập bộ lưu trữ flash thông qua bus PCI, NVMe hỗ trợ hàng chục nghìn hàng đợi song song, mỗi hàng có khả năng hỗ trợ hàng chục nghìn lệnh đồng thời.

#### 3.2. Pool, volume, LUN, disk

**3.2.1.Pool**

* Là một kho lưu trữ tổng hợp nhiều đĩa vật lý thành một không gian lưu trữ lớn.
* Từ một kho lưu trữ, có thể tạo một hoặc nhiều đĩa ảo. Các đĩa ảo này cũng được gọi là không gian lưu trữ.

Lợi ích:

* Có thể tạo nhiều volume trên pool, cho phép phân chia không gian lưu trữ giữa người dùng và ứng dụng.
* Nếu một ổ đĩa bị mất hoặc được thêm vào, trình quản lý hệ thống sẽ tự động cân bằng lại dữ liệu trên tất cả các ổ đĩa đang hoạt động mà không làm ảnh hưởng đến dịch vụ bên trong.
* Qtier cung cấp tự động phân tầng khi nhóm lưu trữ chứa hỗn hợp các đĩa SATA, SAS và SSD.
* Snapshots chỉ được sử dụng với pool storage. Snapshots ghi lại trạng thái của dữ liệu trên một ổ đĩa hoặc LUN tại một thời điểm cụ thể.

Dữ liệu sau đó có thể được khôi phục vào thời điểm đó nếu nó vô tình bị sửa đổi hoặc xóa.

**3.2.2.Volume**

* Volume: lưu trữ dữ liệu, quản lý và sắp xếp không gian lưu trữ.
* Được tạo từ capacity lưu trữ có sẵn giúp tổ chức và sử dụng tài nguyên hệ thống một cách hợp lý, truy cập một cách dễ dàng và nhanh chóng.

(capacity - là lượng dữ liệu có thể lưu trữ trong một ổ đĩa)

* Volume là lớp dữ liệu duy nhất hiển thị cho máy chủ. Một volume có thể chứa: ứng dụng, databases, file system.
* Trong SAN, các volumes được ánh xạ tới LUN (các số đơn vị logic). Các LUN giữ dữ liệu người dùng có thể truy cập bằng cách sử dụng một hoặc nhiều giao thức truy cập máy chủ được hỗ trợ bởi mảng lưu trữ, bao gồm FC, iSCSI và SAS.

**3.2.3. LUN (logical unit number)**

* LUN là một loạt các khối được cung cấp từ một nhóm lưu trữ được chia sẻ và được trình bày cho máy chủ dưới dạng một đĩa logic.
* Là mã định danh duy nhất để chỉ định thiết bị lưu trữ vật lý hoặc thiết bị ảo thực thi các lệnh vào ra (I/O) với máy chủ.
* Mỗi máy chủ lưu trữ có không gian địa chỉ LUN riêng. Do đó, cùng một LUN có thể được sử dụng bởi các máy chủ khác nhau để truy cập các volume khác nhau.
* LUN có thể tham chiếu toàn bộ RAID, một ổ đĩa, một phân vùng hoặc nhiều ổ đĩa và phân vùng.
* LUN có thể đơn giản việc quản lý tài nguyên lưu trữ do quyền truy cập và kiểm soát có thể được chỉ định thông qua các định danh logic.
* Có thể sử dụng để phân vùng và làm mask trong SAN hoặc được ảo hóa để ánh xạ nhiều LUN vật lý.

**Các loại của LUN:**

1. Mirrored LUN: để dự phòng dữ liệu và sao lưu.
1. Concatenated LUN: Gộp nhiều LUN thành đơn vị hoặc volume duy nhất.
1. Striped LUN: Ghi dữ liệu trên nhiều ổ đĩa vật lý, có khả năng nâng cao hiệu suất bằng cách phân phối các yêu cầu I / O trên các ổ đĩa.
1. Striped LUN with parity: Truyền dữ liệu và nếu một ổ đĩa vật lý bị lỗi, dữ liệu có thể được xây dựng lại từ thông tin trên các ổ đĩa còn lại.

**LUN zoning and masking**

* SAN kiểm soát quyền truy cập máy chủ vào LUN để thực thi bảo mật và toàn vẹn dữ liệu.
* LUN zoning : cung cấp các đường dẫn riêng biệt cho I / O thông qua kết cấu FC SAN giữa các cổng cuối để đảm bảo hành vi xác định.
* LUN masking : hạn chế quyền truy cập máy chủ vào các mục tiêu SCSI được chỉ định và LUN. Với LUN masking, một số máy chủ và vùng có thể sử dụng cùng một cổng trên thiết bị lưu trữ.

**3.2.4. Disk**

* Dữ liệu được lưu trữ trên mảng đĩa (disk array)

**Disk & solid state drive (SSD)** : dung lượng chia sẻ cho nhiều máy chủ, có khả năng đạt được IO cao với độ trễ cực thấp.

* 3PAR là nhà cung cấp hệ thống đĩa cao cấp đầu tiên trên thị trường tận dụng thực tế là 90% IO trên hầu hết các hệ thống dưới 10% số khối.

**Disk array controller (flash):**

* disk array: Là hệ thống đĩa lưu trữ nhiều ổ đĩa, array có bộ đệm cache và các chức năng nâng cao như : RAID, sao chép, mã hóa và ảo hóa.
* Là thiết bị quản lý các ổ đĩa vật lý và trình bày chúng cho máy tính dưới dạng các đơn vị logic, luôn luôn thực hiện RAID phần cứng, đôi khi nó được gọi là bộ điều khiển RAID.
