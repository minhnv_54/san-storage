#### 1. Các khái niệm cơ bản: IO, IOPS, latency, block size, IO size
##### a. IO - Nhắc tới hiệu suất


- Là giao tiếp giữa mảng lưu trữ và máy chủ lưu trữ.


- Đầu vào là dữ liệu được nhận bởi mảng và đầu ra là dữ liệu được gửi từ nó.


- Để phân tích và điều chỉnh hiệu suất, bạn phải hiểu khối lượng công việc mà ứng dụng hoặc máy chủ đang tạo. Dựa vào:


        1. Kích thước IO: kích thước IO càng lớn thì băng thông lưu trữ càng cao.

	2. Mẫu truy cập IO:

            * Đọc / ghi ngẫu nhiên - đọc và ghi hoàn toàn ngẫu nhiên,không thể tăng hiệu suất với bộ đệm.
            * Đọc / ghi tuần tự - truy cập hoàn toàn tuần tự, sử dụng bộ nhớ cache để tăng hiệu năng.

        3. Số luồng: nhiều IO sẽ có một hàng đợi để tối ưu cách thức hoạt động.

	4. Tỷ lệ đọc ghi:

           * Đọc ngẫu nhiên thì sẽ không lưu trong bộ nhớ cache và phải nạp tìm từ ổ đĩa cứng.
           * Ghi từ bộ nhớ cache vào ổ cứng giúp tối ưu hóa hệ thống lưu trữ
##### b. IOPS ( Input / Output Operations Per Second - i-ops)


- IOPS - hoạt động vào / ra trên mỗi giây


- Là phép đo hiệu suất phổ biến sử dụng để đánh giá các thiết bị lưu trữ máy tính như ổ đĩa HDD, SSD, SAN.


- IOPS có thể đo bằng các UD như FIO, sử dụng với các máy chủ để tìm cấu hình lưu trữ tốt nhất.


- Yếu tố khác cũng có thể ảnh hưởng đến kết quả IOPS bao gồm thiết lập hệ thống, trình điều khiển lưu trữ, hệ điều hành.


##### c. Latency - Độ trễ


Là thời gian cần thiết để yêu cầu đi từ người gửi đến người nhận và để người nhận xử lý yêu cầu đó.


Độ trễ xác định mức độ nhanh chóng yêu cầu gửi đi từ khách đến máy chủ và ngược lại.


Độ trễ tạo ra các nút cổ chai trong mạng -> giảm dữ liệu có thể được truyền trong một khoảng thời gian.


- **4 nguyên nhân ảnh hưởng tới độ trễ **:
1. Các phương tiện truyền dẫn

2. Lượng thời gian 1 gói đi từ nguồn này tới nguồn khác

3. Bộ định tuyến cần thời gian để phân tích thông tin của gói

4. Sự chậm trễ bởi các thiết bị trung gian như chuyển mạch và cầu nối.

- **Cách giảm độ trễ**:

Giảm lượng thời gian trễ của máy chủ sẽ giúp tải tài nguyên web của bạn nhanh hơn, cải thiện thời gian tải trang khi truy cập
   1. Mở rộng HTTP requests: Giảm số lượng số lượng yêu cầu HTTP không chỉ áp dụng cho hình ảnh mà còn cho các tài nguyên bên ngoài.

   2. Sử dụng CDN: mang tài nguyên đến người dùng bằng cách lưu trữ nhiều nơi trên thế giới. Tài nguyên sẽ đc lưu vào cache và mỗi khi nguời dùng yêu cầu nó sẽ tìm đến điểm gần nhất mà không cần quay lại máy chủ mỗi lần.

   3. Sử dụng các phương pháp tìm nạp trước: cải thiện hiệu suất nhận biết của trang web khi họ nhấp vào trang tiếp theo, các công việc như tra cứu DNS đã được thực hiện, do đó tải trang nhanh hơn.

   4. Bộ nhớ đệm trình duyệt: Các trình duyệt sẽ lưu trữ một số tài nguyên nhất định của một trang web cục bộ để giúp cải thiện thời gian trễ và giảm số lượng yêu cầu quay lại máy chủ.

##### d. Block size - Kích thước khối


Trên SAN, files được chia thành các khối dữ liệu có kích thước đồng đều và kích thước của mỗi khối được xác định bởi khả năng phần cứng của nhà sản xuất.


Block size có thể thay đổi nếu lưu trữ đĩa tiêu chuẩn được sử dụng và việc chọn kích thước khối chính xác cho dữ liệu khách hàng là rất quan trọng để đảm bảo hiệu suất tối đa.


Mỗi khối dữ liệu sau đó được hệ điều hành cấp cho một ID vị trí để theo dõi vị trí của từng khối dữ liệu được ghi trên SAN.
