### 2. Benchmark IOPS

#### 1. Đo ngẫu nhiên IOPS với FIO

* Khi chạy 1 trang web, phép đo tốt nhất của hệ thống đĩa là IOPS (Hoạt động vào / ra mỗi giây)

=> FIO là một công cụ phổ biến để đo IOPS trên máy chủ Linux.

**Thực hiện tải xuống và biên dịch nó:**

```shell
cd /root
yum install -y make gcc libaio-devel || ( apt-get update && apt-get install -y make gcc libaio-dev  </dev/null )
wget https://github.com/Crowd9/Benchmark/raw/master/fio-2.0.9.tar.gz ; tar xf fio*
cd fio*
make
```

\-> FIO được biên dịch theo các testcase sau:

**1. Đo IOPS**

*a. Random Read*

```shell
B1: Tạo file fio_random_read.fio

[global]
bs=4K
iodepth=256
direct=1
ioengine=libaio
group_reporting
time_based
runtime=120
numjobs=4
name=iops-test-job
rw=randread

[job1]
filename=/tmp/test
filesize=1m



B2: Run command

fio fio_random_read.fio
```

**INPUT**

**\[global\]**: mô tả công việc được đặt trong tệp đó.

**-- direct**: Linux hỗ trợ I / O (set direct = 1 (không được đệm) hoặc direct = 0 (được đệm)

**-- bs**: là kích thước khối tính theo byte được sử dụng cho các đơn vị I / O, mặc định: 4096 = 4k.

**--iodepth**: độ sâu của hàng đợi, tạo ra các công việc song song, với iodepth càng lớn thì IOPS (số lần vào ra I/O trên mỗi giây) sẽ càng lớn

Một công việc tuần tự với iodepth = 2 sẽ gửi hai yêu cầu IO liên tiếp tại một thời điểm.

Một công việc tuần tự với numjobs = 2 sẽ có hai luồng, mỗi luồng gửi IO tuần tự.

**-- ioengine**: Xác định cách thức phát hành I/O cho mỗi tệp.

Với --ioengine=libaio -> Linux hỗ trợ I/O không được đệm

**group_reporting**: Để xem báo cáo cuối cùng cho mỗi nhóm thay vì theo từng công việc

**runtime & time_based**:

* runtime = 30 : Yêu cầu fio chấm dứt xử lý sau khoảng thời gian được chỉ định, tính bằng giây.
* time_based: lặp trên cùng một khối lượng công việc nhiều lần trong thời gian chạy cho phép.

**numjobs**: số luồng thực hiện công việc giống nhau và tạo nên goup_reporting.

**name**: Đối với mỗi tùy chọn --name mà fio nhìn thấy, nó sẽ bắt đầu một công việc mới với tên đó.

**rw**: có 6 loaị : read, rite, randread, randwrite, randrw, readwrite ( đọc/ghi tuần tự )

read && randread : 100% read

write && randwrite: 100% write

randrw: 90% read - 10% write ( đọc 9 lần mới thực hiện ghi 1 lần ), ta có thể điều chỉnh số lần đọc ghi như sau:

**--rwmixread=75** : 75% read - 25% write (3 lần đọc mới thực hiện ghi 1 lần )

**\[job\]**:

**-- filename**: Nếu muốn chia sẻ tệp giữa các luồng trong một công việc hoặc một số công việc có đường dẫn tệp cố định => chỉ định tên tệp cho mỗi luồng để ghi đè mặc định.

**--filesize**: kích thước của file.

**OUTPUT**

![image](/uploads/09417302c8fe74dbb73b1a07eb231d21/image.png)

### Đọc hiểu output của fio

```
Jobs: 4 (f=4): [r(4)][100.0%][r=15.7MiB/s,w=0KiB/s][r=4013,w=0 IOPS][eta 00m:00s]
```

* Ký tự bên trong dấu ngoặc vuông đầu tiên biểu thị trạng thái của từng luồng.

r(4) : trạng thái running, random read.

```
read: IOPS=5113, BW=19.0MiB/s (20.9MB/s)(2397MiB/120016msec)
```

IOPS : số I / O trung bình được thực hiện mỗi giây.

BW : tốc độ băng thông trung bình

Hai giá trị cuối hiển thị: tổng I / O được thực hiện với 2 định dạng / thời gian chạy của luồng đó.

```
slat (usec): min=2, max=99632, avg=778.34, stdev=2973.07
```

**slat**: Đây là thời gian cần thiết để gửi I / O ( nhỏ nhất, lớn nhất, trung bình, độ lệch ).

Giá trị này có thể tính bằng nano giây, micro giây hoặc mili giây - fio sẽ chọn cơ sở phù hợp nhất và in ra (trong ví dụ trên nano giây là tỷ lệ tốt nhất).

```
clat (msec): min=5, max=525, avg=199.21, stdev=80.74
```

**clat**: thời gian từ khi gửi đến khi hoàn thành I / O. Để I / O đồng bộ hóa, clat thường sẽ bằng (hoặc rất gần) với 0.

```
lat (msec): min=16, max=525, avg=199.99, stdev=81.07
```

**lat** : Đây là thời gian chuyển giữa trình gửi tới kernel và khi IO hoàn thành, không bao gồm độ trễ gửi, là số liệu tốt nhất để tính gần đúng độ trễ cấp ứng dụng.

```
clat percentiles (msec):
     |  1.00th=[   35],  5.00th=[   42], 10.00th=[   53], 20.00th=[  102],
     | 30.00th=[  205], 40.00th=[  215], 50.00th=[  224], 60.00th=[  230],
     | 70.00th=[  236], 80.00th=[  247], 90.00th=[  271], 95.00th=[  296],
     | 99.00th=[  372], 99.50th=[  401], 99.90th=[  460], 99.95th=[  477],
     | 99.99th=[  502]
```

1% trong số các yêu cầu đã thực hiện 35 msec

5% trong số các yêu cầu đã thực hiện 42 msec, ...

99\.99% yêu cầu thực hiện trong 502 msec.

```
 bw (  KiB/s): min= 2336, max=21344, per=24.96%, avg=5105.56, stdev=2994.05, samples=960
```

* **per** : tỷ lệ phần trăm tổng băng thông mà luồng này nhận được trong nhóm.
* **samples**: số lượng mẫu được lấy, giá trị cuối cùng này chỉ thực sự hữu ích nếu các luồng trong nhóm nằm trên cùng một đĩa, khi đó chúng đang cạnh tranh để truy cập đĩa.

```
iops        : min= 584, max= 5336, avg=1276.38, stdev=748.51, samples=960
```

* Thống kê IOPS (thời gian thực hiện I/O trên mỗi giây) dựa trên samples.

```
lat (msec)   : 10=0.01%, 20=0.01%, 50=9.25%, 100=10.72%, 250=61.91%

lat (msec)   : 500=18.10%, 750=0.01%
```

**lat**: Việc phân phối độ trễ hoàn thành I / O.

0\.01% I/O hoàn thành dưới 10 msec, 0.01% I/O cần 10-19 msec để hoàn thành.

```
cpu          : usr=0.56%, sys=1.15%, ctx=61117, majf=0, minf=1057
```

**cpu**: usr && sys - thời gian của người dùng và hệ thống

ctx: số lượng chuyển đổi ngữ cảnh mà luồng này đã trải qua

major && minor : số lỗi trang chính và phụ.

```
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=0.1%, 32=0.1%, >=64=100.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.1%
     issued rwt: total=613644,0,0, short=0,0,0, dropped=0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=256
```

**IO depths**: Hàng đợi I/O, các số được chia thành lũy thừa của 2.

**IO submit**: I/O được gửi trong một lần gửi. VD: 4=100.0% tức là có 1 -> 4 I/O được gửi cho mỗi lần gửi

**IO complete**: I/O nhận được khi goàn thành.

**IO issued rwt**: số lượng read/write/trim , bao nhiêu trong số chúng được rút ngắn hoặc bỏ.

**IO latency**: các option trong IO latency dành cho latency_target, khi tùy chọn được kích hoạt IO depths cần thiết cho latency được chỉ định.

#### Sau khi liệt kê số liệu

```
Run status group 0 (all jobs):
   READ: bw=19.0MiB/s (20.9MB/s), 19.0MiB/s-19.0MiB/s (20.9MB/s-20.9MB/s), io=2397MiB (2513MB), run=120016-120016msec
```

**bw**: giá trị đầu tiên là băng thông tổng hợp của các luồng trong group, tiếp theo là băng thông tối thiểu và tối đa

**io**: I/O được thực hiện của tất cả các luồng trong group.

**run**: Thời gian chạy nhỏ nhất và lớn nhất của các luồng trong group.

```
Disk stats (read/write):
  sda: ios=384237/1005, merge=203956/1046, ticks=7149287/31872, in_queue=6417568, util=96.42%
```

**ios**: số lượng I/Os được thực hiện bới tất cả các group

**merge**: Số lượng hợp nhất được thực hiện bởi bộ lập lịch I / O.

**ticks**: Số lượng ticket giữ cho đĩa bận.

**in_queue**: Tổng thời gian chờ

**util**: Việc sử dụng đĩa, 100% - luôn bận, 50% - nửa thời gian chờ của đĩa.

#### 2. Benchmark với IOstat\*\*

```
$ iostat -m 20
```

#### INPUT

* Tham số -m : hiển thị số liệu tính bằng MB/s ( thay vì block hay KB/s), 20s update lại 1 lần.
* **iostat**: sử dụng để báo cáo tốc độ đọc / ghi của đĩa và đếm trong một khoảng thời gian liên tục. Thu thập số liệu thống kê của đĩa, chờ trong khoảng thời gian nhất định, thu thập lại và hiển thị sự khác biệt.
* **iostat** : thu thập dữ liệu ở lớp khối nhân Linux và do đó phản ánh IO thực của đĩa vật lý.

#### OUTPUT

![image](/uploads/c43997753b5b231dc9aa1c261763f89d/image.png)

* Số liệu thống kê CPU và số liệu thống kê đĩa.

```
avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           1,76    0,00    1,36    0,20    0,00   96,68
```

\-> Các số liệu thống kê CPU là một sự chia nhỏ về thời gian CPU đã được sử dụng trong khoảng thời gian.

**user**: %cpu người dùng sử dụng

**nice**: % sử dụng CPU cho các ứng dụng có mức độ ưu tiên

**system**: % thời gian trong đó CPU không hoạt động nhưng có yêu cầu I/O nổi bật

**steal**: % thời gian CPU đang chờ vì trình ảo hóa đang hoạt động trên CPU khác

**idle**: % thời gian hệ thống không hoạt động mà không có yêu cầu nổi bật.

```
Device             tps    MB_read/s    MB_wrtn/s    MB_read    MB_wrtn
loop0             0,00         0,00         0,00          0          0
loop1             0,00         0,00         0,00          0          0
```

* Số liệu thống kê đĩa bao gồm số lượng yêu cầu I / O mỗi giây (tps), tốc độ đọc và ghi (kB_read / s và kB_write / s) và lượng dữ liệu đọc và ghi (kB)

=> Để biết được số IOPS, nhìn vào tps = r / s + w / s

#### Câu lệnh iostat khác để check IOPS

* Để lấy được số IOPS

```
iostat -dx sda | grep sda | awk '{ print $4; }' => 245,57


iostat -dx sda | grep sda | awk '{ print $4"/"$5; }' => 246,27/264,77
```

field 4 : reads/second, field 5: writes/second.
