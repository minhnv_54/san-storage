# Zoning

### Zoning là gì cần sao cần zoning

Khi đọc về SAN và Storage ta thường nghe đến khái niệm zones và zoning. Với các
hệ thống có sử dụng SAN dữ liệu được lưu trữ tập trung trên SAN và các host kết 
nối đến SAN thông qua San Switch. Zoning cho phép tạo 1 path từ host đến SAN. 
Nói cách khác zoning sẽ khoanh vùng mạng fabric 1 các hợp lý. 

Fiber Channel switch (SAN Switch) là một thiết bị mạng cho phép nhiều host cùng 
kết nối đến 1 hoặc nhiều storage array (SAN). Một host cũng có thể kết nối trực
tiếp đến storage array nhưng điều này sẽ hạn chế số lượng vì số lượng FC port 
trên SAN rất ít, như vậy San Switch là cần thiết để 1 lượng lớn host có thể kết
nối đến storage array. 

Zoning có thể tránh được các lưu lượng không cần thiết giữa các thiết bị cùng 
kết nối đến SW và tăng cường tính bảo mật. Zoning tạo ra các phân vùng hợp lý để
thiết bị trong cùng 1 zone mới có thể giao tiếp với nhau.

**Zoning**

Mỗi device fiber channel fabric đều có 1 WWN (Word Wide Name) định danh duy nhất
trên thế giới. WWN là duy nhất được ghi vào phần cứng (giống với MAC trên các
NIC).

Có 2 loại WWN:
 - Word Wide Node Name (WWNN)
 - Word Wide Port Name (WWPN)

1 WWPN của HBA trên host và 1 WWNN của 1 port FC trên control của SAN sẽ tạo 
thành 1 zone.

Show WWN trên server

```
[root@hlc6fcom-29 ~]# cat /sys/class/fc_host/host*/port_name 
0x51402ec0110f0220
0x51402ec0110f0222
```

Show WWN trên SAN

![image](/uploads/38405c436a6ad25cfdedb730b2726ca4/image.png)

```
  zone name openstack_sansw07_210034800d6d91a05006016a49e0413b vsan 10
  * fcid 0x651500 [pwwn 21:00:34:80:0d:6d:91:a0]
  * fcid 0x660100 [pwwn 50:06:01:6a:49:e0:41:3b]
```

Như ví dụ trên là 1 zone trên SW

Trong đó zone có tên là `openstack_sansw07_210034800d6d91a05006016a49e0413b`

 - `21:00:34:80:0d:6d:91:a0` là WWN của port HBA trên server
 - `50:06:01:6a:49:e0:41:3b` là WWN của port FC trên control của SAN

Như vậy mỗi đường từ host đến SAN thì trên SW sẽ tạo thành 1 zone. 1 WWN có thể
thuộc 1 hoặc nhiều zone.
