# Multipath

### Multipath là gì ?

Một path là một kết nối từ máy chủ đến thiết bị lưu trữ (SAN).
Kết nối này có thể xảy ra vấn đề khi card HBA trên server bị lỗi, 
dây cap có vấn đề,... Để đảm bảo tính dự phòng cho hệ thống thì 
nên có nhiều hơn 1 đường kết nối từ server xuống dưới SAN từ đây
`multipath` ra đời. Multipath đảm bảo hệ thống có nhiều đường kết 
nối vật lý từ server xuống SAN để vừa đảm bảo tính dự phòng vừa tăng
băng thông cho hệ thống. 

![image](/uploads/f26894fe3df9e2b96c9fcaee3607d78b/image.png)

Như trên hình ta thấy có 4 path từ server xuống SAN.

### Device mapper multipathing

Như hình bên trên là cắm fabric kết nối từ server xuống tủ đĩa, nên sẽ có 4 I/O 
path từ server xuống tủ đĩa. Để tận dụng gom nhóm các path này lại như nào, xử 
lý khi có lỗi ra sao thì cần có dm-multipath.

Fabric là cách cắm mà mỗi node sẽ được nối đến 1 hoặc nhiều switch(giống cách cắm
như mô hình bên trên)

![image](/uploads/12684a2a7f8a5b38c823c0fdc5a330b8/image.png)

Mesh là cách cắm mà cách switch trong mạng fabric được nối đến nhau.

![image](/uploads/aff91c7c0a0245bb00324781b2da425d/image.png)

Thành phần

| Component | Description |
| --------- | ----------- |
| dm-multipath | kernel module chịu trách nhiệm quyết định định tuyến trong điều kiện bình thường hoặc failure |
| multipath | Lệnh được sử dụng để xem/liệt kê các multipath device và để cấu hình ban đầu |
| multipathd | Daemon được sử dụng để monitor đường dẫn, đánh dấu các đường dẫn bị lỗi, kích hoạt lại các đường dẫn đã khôi phục, thêm/xóa các device file khi cần |
| kpartx | Lệnh được sử dụng để tạo các mục ánh xạ thiết bị cho các phân vùng trên LUN. Nó được gọi tự động khi sử dụng lệnh multipath |

### Một số câu lệnh multipath

**Cấu hình multipath**

File cấu hình multipath nằm ở file `/etc/multipath.conf`

```
(multipathd)[root@hlc6fcom-29 /]# cat /etc/multipath.conf 
defaults {
  polling_interval 10
  user_friendly_names yes
  find_multipaths yes
  flush_on_last_del yes
}
multipaths {
  multipath {
    wwid 360021868018107e0c000000000000098
    alias mpatha
  }
}
devices {
  device {
    vendor "3PARdata"
    product "VV"
    path_grouping_policy "group_by_prio"
    path_selector "service-time 0"
    path_checker tur
    features "0"
    hardware_handler "1 alua"
    prio "alua"
  }
}
```

 - `polling_interval`: Khoảng thời gian giữa 2 lần kiểm tra trạng thái của path
 (tính bằng giây)
 - `user_friendly_names`: Để `yes` nếu bạn muốn đặt alias cho WWID của multipath
 hoặc `no` thì mặc định sẽ sử dụng WWID thay vì tên alias.
 - `find_multipaths`: Định nghĩa mode để setup multipath device. Nếu giá trị là `yes`
 thì multipath ko tạo tất cả các device cho tất cả các path mà nó thấy. Multipath 
 chỉ tạo device khi thuộc 1 trong 3 trường hợp sau:
    + Có ít nhất 2 path không thuộc blacklist(được khai báo trong file /etc/multipath/wwids)
     mà có cùng WWID.
    + Người dùng force create device bằng lệnh multipath
    + Một path có cùng WWID như một multipath device đã được tạo trước đó. Bất cứ khi nào một multipath device được tạo với find_multipaths, multipath sẽ ghi nhớ WWID của thiết bị để nó sẽ tự động tạo lại thiết bị ngay khi nhìn thấy một path với WWID đó.

 - `flush_on_last_del`: khi được set thành yes thì multipath daemon sẽ disable queue
 khi path cuối cùng bị xóa.

Trong group devices có các option:

 - `vendor`: chỉ ra tên nhà cung cấp storage device
 - `product`: tên của storage
 - `path_grouping_policy`: Chính sách để group các path. Có các option
    + failover: 1 path cho mỗi priority group.
    + multibus: tất cả các path hợp lệ trong 1 priority group.
    + group_by_serial: 1 priority group cho mỗi serial number.
    + group_by_prio:  1 priority group cho mỗi giá trị path priority.
    + group_by_node_name:  1 priority group cho mỗi target node name. Target node names được lấy trong file `/sys/class/fc_transport/target*/node_name`.

 - `path_selector`: Thuật toán được sử dụng để xác định path sẽ được sử dụng cho
 I/O tiếp theo.
 - `path_checker`: Chỉ định method để kiểm tra trạng thái của path.
 - `features`: Các tính năng bổ sung của multipath device.
 - `hardware_handle`: Chỉ định một mô-đun sẽ được sử dụng để thực hiện các hành 
 động cụ thể của phần cứng khi chuyển đổi path group hoặc xử lý lỗi I/O. 
 - `prio`: chỉ định function để lấy giá trị ưu tiên của path.

Tham khảo thêm thông tin cấu hình tại [đây](http://www.datadisk.co.uk/html_docs/redhat/rh_multipathing.htm) hoặc [đây](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/dm_multipath/config_file_multipath)

**List thông tin các path**

```
multipath -ll
```

Output

- Path device

```
action_if_any: alias (wwid_if_different_from_alias) dm_device_name_if_known vendor,product size=size features='features' hwhandler='hardware_handler' wp=write_permission_if_known
```

- Path group

```
policy='scheduling_policy' prio=prio_if_known status=path_group_status_if_known
```

- Path

```
host:channel:id:lun devnode major:minor dm_status_if_known path_status online_status
```

  * **dm-status** có trạng thái `failed` và `active`
  * **path_status** có trạng thái là `ready` nếu path up và sẵn sàng cho đọc ghi hoặc `ghost` nếu path up khi path đang ở trạng thái standby .Và có trạng thái `faulty` nếu path down hoặc `shaky` khi path đã up nhưng ko có khả năng có các hoạt động bình thường.
  * **online_status** có trạng thái running hoặc offline.

VD:

![image](/uploads/d9646f41117788a10448ff1cad20b8ed/image.png)

**show the current multipath topology from information fetched in sysfs and the device mapper**

```
# multipath -l 
360002ac000000000000004720002107b dm-15 3PARdata,VV              
size=1.0T features='1 queue_if_no_path' hwhandler='1 alua' wp=rw
`-+- policy='service-time 0' prio=0 status=active
  |- 0:0:0:13 sddf 70:208  active undef running
  |- 0:0:1:13 sdde 70:192  active undef running
  |- 0:0:2:13 sddc 70:160  active undef running
  |- 0:0:3:13 sddd 70:176  active undef running
  |- 3:0:0:13 sddg 70:224  active undef running
  |- 3:0:1:13 sddj 71:16   active undef running
  |- 3:0:2:13 sddi 71:0    active undef running
  `- 3:0:3:13 sddh 70:240  active undef running
```

Option này sẽ không thể hiện được đầy đủ thông tin như option `-ll`

**Xóa 1 path chỉ định**

```
multipath -f 360002ac000000000000004720002107b
```

**Xóa tất cả path**

```
multipath -F
```
